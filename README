		The Roguelike Server

	The software is intented for hosting text-mode games,
roguelikes in particular to be available for the remote players to via
telnet protocol. As an additional feature, other users may observe
upon game sessions in real-time.


		Running a server

	By default, the server is configured to be run inside a	dedicated
directory, where its data is stored. So the start-up script must change to
that directory prior to launching the software.

	Although, it is possible to change all file names in sources to
include absolute paths to make it able to run from any directory.

	The server uses a custom termcap entry for internal terminal
emulation. It should be compiled using tic command
		tic term.rls
	by default, the resulting compiled binary description is placed
into ~/.terminfo You need to copy it to some place accessible for the server.
Unless you put it to the standard location (usually /usr/share/terminfo),
you should use TERMINFO environment variable to point to the location.

	Environment variable TERM=rls must be set in order for the remote
playing and observing to work properly.

	At the start, the software tries to read the config file named
'confg'. It defines a TCP/IP port to listen for incoming telnet connections,
a command to be used to run external editor to edit config files, and
other settings.

	The suggested editor to use is easyedit. There is a patch provided
to disable 'Save As' and 'Print' functionality.

	Server continues to run until it receives SIGTERM. Then it gracefully
tries to terminate all running games (by sending SIGTERM to them) and close
network connections.

	To set up the server as daemon, use an init script template (usually
found in /etc/rc.d/init.d/) and fill with something like the following to
start the server
		cd /home/rlserver	# or where did you place it
		TERMINFO=/home/rlserver/.terminfo TERM=rls ./rlserver
it is likely there is a mechanism provided for terminating daemon, if not
you can simply use
		killall rlserver
as long as you are reasonably sure there are no other processes
named 'rlserver'.

	Server uses syslog utility to write logs.


		Security considerations

	While the server was developed to minimise exposure to remote
users, its very nature of running arbitraty software makes it impossible
to achieve the task.

	If possible, a dedicated chroot jail should be used. If that is
impractical in some particular case, server should be run with mimimal
user privilegies.

	In any case, the user account used to run server or games should not
have write permissions other than necessary to save games and high score
lists.

	Some games may prompt user for a file name for a save file, character
dump, etc. Malicious players may enter arbitrary file names to damage score
files or saved games of other players. This feature should be disabled for
every game.

	As it advisable to treat the games as untrustworthy malicious code
and users as crackers trying to break in. There can never be too much
paranoia.

	If a game provide some way to disable access to unnecessary
functionality completely, such as sound effects or using an X11 window, it
should be used. It is better to disable such access for the user at the
system level. Just note that using chroot jail may be not enough by itself.

	Most games do not bother with file locking while updating the scores
file, so it is possible for two games run by different players to try to
update it at the same time, leading to corruption. Since there is not much
that can be done about that (aside from patching a game to implement proper
file locking) regular backups seem to be the only measure.

	You might want to use nice in start-up to reduce the priority of the
games relative to other tasks unsing the computer. Also, ulimit (built-in to
bash shell) may be helpfull to limit memory and/or CPU consumption to
prevent a single game from degrading experience for other users.


		Statistics

	If stat file is defined in the config, the sever will track and
update statistics for every game played. Stats file keeps accumulated
counters for several numbers, one per line: number of sessions played, total
time of sessions in seconds, total count of bytes received from the players,
and total count of bytes sent to players (observers are not counted).

	If log file is defined, more detailed data will be written for
every session: time of the session end, user, session duration in seconds,
bytes received from the player and bytes sent to him (observers are not
counted).


		Users

	All user accounts are stored in file 'users' in the server directory
in simple text format.

	It is possible to edit the file manually, but in this case the server
must be offline or the editor must temporarily lock file via POSIX lockf()
to prevent simultaneous writes if a remote player register an account or
changes password.


		Running games

	All game profiles are read from file 'games' in the server directory.
The file is re-read every time a player enters the game selection menu, so
there is no need to restart server to add a new game or edit profile of an
old one.

	Every game has its own section in the file. A typical section may
look like the following:

[myrl]
name: My Roguelike v 0.1.2
chdir: myrl-0.1.2
copy: data/newtemplate.save $USER$.save
copy-edit: data/keys keys-$USER$
run: ./myrl --user $USER$
score: ./myrl --showscore
enter: lf

	The first line in brackets denotes section start. The string inside
brackets is the game id. It will be used when players are selecting a game
to run in the server menu. The following directives may be used in any
order.

	Mandatory parameter name: specifies the game name to be displaed in
server menus.

	Mandatory run: defines a command to run the game. All instances of
substring $USER$ will be replaced with player's login name.

	Optional chdir: specifies game direcory. It may be relative to the
server direcory or an absolute path. The game will be run from this
directory.

	Optional copy: makes a copy of specified file prior to running a
game if the file does not exist. It can be used to make personal copies
of game configurations. If chdir: is defined for the game, the direcory
will be used for both file names.

	Optional copy-edit: is very similar to copy:. The only difference
is that the copied file will be allowed to be edited by player.

	There can be multiple copy: and copy-edit: entries for a single
game. By default the limit for both combined is 8. It can be changed by
editing GAME_FILES constant in games.h and recompiling.

	Optional enter: specifies how to translate the Enter key character
prior to passing the input to player. Possible values are LF (default) for
'\n' and CR for '\r'. Try setting this option if some game does not
recoginse the ENTER key.

	Optional score: defines a command to run game in view-scores mode.
The command should run the game in a mode where it does not allow any other
action than browse score lists.


		Game requirements

	In order for games to work properly with the server, they should meet
some requirements:

	If game allows to save progress, it should support multipe
independent save file by accepting a name from command line and do not
allow to change it from within the game itself.

	As mentioned in the security section, a game should not allow player
to enter arbitrary file names for any purpose.

	A game should save data and exit gracefully upon receiving SIGTERM.
This signal is sent when remote players disconnects from the game or when
the server is going through shutdown.

	If game writes to files other than personalised saves and configs,
it should employ some file locking mechanism to protect these file from
corruption due to simultaneous writes initiated by different players.

	It is not necessary, but if a game has a command-line option to
display a high-score list, it can be used. The game should exit after
displaying the list and do not allow any other actions, such as starting
to play.


		Ending

	The rlserver is developed by Ilya Belyy (ilyabely@gmail.com).
Special thanks goes to Glowie for without his enthusiasm and support the
project could not be started nor developed further.

	The software is distributed on terms of the GNU GPL v3 or any
later version to be chosen by user.
