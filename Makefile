CC = gcc
CFLAGS += -pipe -Wall -Wcast-align -Wshadow -Wpointer-arith -Wcast-qual -Wmissing-prototypes -Werror-implicit-function-declaration \
	-O2 -g2 \
	-DUSE_SYSLOG
LDFLAGS += -pipe -Wall -O0 -g2 -lutil -lpthread

OBJDIR = ./obj

SERVER = ./rlserver
OBJS = $(OBJDIR)/util.o \
	$(OBJDIR)/telnet.o $(OBJDIR)/vt100.o \
	$(OBJDIR)/sessions.o $(OBJDIR)/run.o \
	$(OBJDIR)/menu.o $(OBJDIR)/users.o $(OBJDIR)/games.o \
	$(OBJDIR)/stats.o $(OBJDIR)/syslog.o $(OBJDIR)/server.o


.SUFFIXES: .c
.PHONY: clean depend all

all: $(OBJDIR) $(SERVER)

$(SERVER): $(OBJS)
	$(CC) -o $@ $^ $(LDFLAGS)

$(OBJDIR)/%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<


clean:
	rm -rf $(SERVER) $(OBJS)
	rm -rf $(OBJDIR)/Makefile.depend*

depend: $(OBJDIR)/Makefile.depend

$(OBJDIR)/Makefile.depend: Makefile *.c *.h
	touch $@
	makedepend -p$(OBJDIR)/ -f$@ -Y 2>/dev/null *.c

$(OBJDIR):
	mkdir $@

-include $(OBJDIR)/Makefile.depend

