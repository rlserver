#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <libgen.h>
#include <pthread.h>
#include <pty.h>
#include "util.h"
#include "log.h"
#include "stats.h"
#include "telnet.h"
#include "vt100.h"
#include "sessions.h"
#include "server.h"
#include "run.h"


extern server_config config;


/*
 * close everything that is not neccessary before running a game in a child process
 */
static void close_server_things (void)
{
	close_sessions ();

	extern int server_socket;
	close (server_socket);

	// local stdio is not used
	close (0);
	close (1);
	close (2);
}



static void send_to_observers (session_info *sess, const char *buf, int len)
{
	int i;

	pthread_mutex_lock (&sess->mutex);
	for (i = 0; i < sess->observer_count; i++)
	{
		send (sess->observers[i], buf, len, 0);
	}
	pthread_mutex_unlock (&sess->mutex);
}



static void remove_observers (session_info *sess)
{
	const char msg[] = "\033[0m\033[40;37m\r\nThe observed game was disconnected\r\n";
	send_to_observers (sess, msg, sizeof(msg) - 1);

	pthread_mutex_lock (&sess->mutex);
	free (sess->observers);
	sess->observers = NULL;
	sess->observer_count = 0;
	pthread_mutex_unlock (&sess->mutex);
}




#define BUF_SIZE	16*1024
static void play_game (session_info *sess, char enter_char)
{
	char buf[BUF_SIZE];
	fd_set rfds;
	int i, sn = (sess->socket > sess->pty_master) ? sess->socket : sess->pty_master;


	sess->term = init_term(sess->term_wid, sess->term_hgt);

	while (1)
	{
		FD_ZERO (&rfds);
		FD_SET (sess->pty_master, &rfds);
		FD_SET (sess->socket, &rfds);

		// wait until game prints something to output
		// or player sends something over the net
		if (select(sn + 1, &rfds, NULL, NULL, NULL) == -1)
		{
			write_log (LOG_ERROR, "select failed");
			break;
		}

		// got output from the running game
		if (FD_ISSET(sess->pty_master, &rfds))
		{
			int nr = read(sess->pty_master, buf, BUF_SIZE);
			if (nr <= 0) break;

			// send it to the player
			send (sess->socket, buf, nr, 0);

			// update terminal
			if (sess->term != NULL) term_process (sess->term, buf, nr);

			// duplicate data for the observers
			send_to_observers (sess, buf, nr);

			sess->game_stat.bytes_out += nr;
		}

		// got input from the player trough the net
		if (FD_ISSET(sess->socket, &rfds))
		{
			int nr = recv(sess->socket, buf, BUF_SIZE, 0);
			if (nr <= 0)
			{
				write_log (LOG_DEBUG, "user %s disconnects while playing %s", sess->user_name, sess->game);
				break;
			}

			sess->game_stat.bytes_in += nr;

			nr = translate_telnet_input(buf, nr, sess);
			if (nr > 0)
			{
				// replace ENTER key
				for (i = 0; i < nr; i++) if (buf[i] == '\n') buf[i] = enter_char;

				write (sess->pty_master, buf, nr);
			}

			// update activity timestamp
			sess->last_activity = time(NULL);
		}
	}
}



static void set_game_dir (const session_info *sess, const game_info *game)
{
	char buf[BUF_SIZE];


	if (!game->dir[0]) return;

	struct stat st;

	str_replace (buf, BUF_SIZE, game->dir, "$USER$", sess->user_name);

	// create directory if it doesn't exist
	if (stat(buf, &st) != 0)
	{
		write_log (LOG_DEBUG, "creating directory %s", buf);
		make_dir (buf);
	}

	// change to the directory
	chdir (buf);
}



static void set_game_files (const session_info *sess, const game_info *game)
{
	char buf[BUF_SIZE], dir[BUF_SIZE];
	struct stat st;
	FILE *file, *copy;
	int i, sz;


	for (i = 0; i < GAME_FILES; i++)
	{
		if (!game->files[i].file[0]) break;

		// copy file name to bufer, substituting user name
		str_replace (buf, BUF_SIZE, game->files[i].copy, "$USER$", sess->user_name);

		// file already exist
		if (stat(buf, &st) == 0) continue;

		file = fopen(game->files[i].file, "r");
		if (file == NULL)
		{
			write_log (LOG_ERROR, "unable to open file %s for game %s", game->files[i].file, game->id);
			continue;
		}

		// create directory for the new file
		strcpy (dir, buf);
		make_dir (dirname(dir));

		copy = fopen(buf, "w");
		if (copy == NULL)
		{
			write_log (LOG_ERROR, "unable to copy file %s (specified as %s) for game %s", buf, game->files[i].copy, game->id);
			fclose (file);
			continue;
		}

		// actually copy data
		do
		{
			sz = fread(buf, 1, BUF_SIZE, file);
			fwrite(buf, sz, 1, copy);
		} while (sz >= BUF_SIZE);

		fclose (file);
		fclose (copy);
	}
}



#define MAX_ARGS	32
static void exec_game (session_info *sess, const game_info *game)
{
	char buf[BUF_SIZE];
	char *arg[MAX_ARGS];
	int b, a = 0;


	// change to the directory specified in game config
	set_game_dir (sess, game);

	// copy user-specific game files if required by game config
	set_game_files (sess, game);

	// copy command line to bufer, substituting user name
	str_replace (buf, BUF_SIZE, game->cmd, "$USER$", sess->user_name);

	write_log (LOG_DEBUG, "user %s starts game %s", sess->user_name, buf);

	// break buf[] into zero-terminated strings, filling arg[] with pointers to them
	// TODO: add quotes or \ to escape spaces
	arg[a++] = &buf[0];
	for (b = 0; buf[b] != 0; b++)
	{
		if (buf[b] == ' ')
		{
			buf[b] = 0;
			if (buf[b + 1] != 0 && a < MAX_ARGS - 1)
			{
				arg[a++] = &buf[b + 1];
			}
		}
	}

	arg[a] = NULL;

	execvp (arg[0], arg);
}



static void close_game (session_info *sess)
{
	// check if the game has terminated already 
	if (waitpid(sess->child_pid, NULL, WNOHANG) != 0)
	{
		sess->child_pid = 0;
		return;
	}

	if (waitpid(sess->child_pid, NULL, WNOHANG) == 0)
	{
		// wait some time to allow the game to handle the last input
		sleep (config.delay_before_kill);

		// if the game is still running, send SIGTERM
		if (waitpid(sess->child_pid, NULL, WNOHANG) == 0)
		{
			kill (sess->child_pid, SIGTERM);
		}
	}

	sess->child_pid = 0;
	sess->game[0] = 0;
}



void run_game (session_info *sess, const game_info *game)
{
	int pty_master, pty_slave;
	int pid;
	struct winsize ws;


	ws.ws_col = sess->term_wid;
	ws.ws_row = sess->term_hgt;
	if (openpty(&pty_master, &pty_slave, NULL, NULL, &ws) == -1)
	{
		write_log (LOG_ERROR, "openpty failed");
		return;
	}

	if ((pid = fork()) < 0)
	{
		write_log (LOG_ERROR, "fork failed");
		close (pty_slave);
		close (pty_master);
		return;
	}

	// child
	if (pid == 0)
	{
		close_server_things ();

		dup2 (pty_slave, 0);	// reassign stdin to pty
		dup2 (pty_slave, 1);	// reassign stdout to pty
		dup2 (pty_slave, 2);	// reassign stderr to pty
		// these are not needed anymore
		close (pty_slave);
		close (pty_master);

		exec_game (sess, game);

		// exec should never return
		write_log (LOG_ERROR, "exec in child failed");
		return;
	}


	// parent

	close (pty_slave);

	sess->pty_master = pty_master;
	sess->child_pid = pid;
	strcpy (sess->game, game->name);
	sess->game_stat.start_time = time(NULL);
	sess->game_stat.bytes_in = sess->game_stat.bytes_out = 0;

	play_game (sess, game->enter);

	// update statistics if it's a real playing session (not editor or scores)
	if (sess->observer_count != -1) update_stats (game, sess);

	remove_observers (sess);

	close_game (sess);

	sess->pty_master = -1;
	close (pty_master);
}

