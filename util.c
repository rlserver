#include <sys/stat.h>
#include <string.h>
#include "util.h"



void str_replace (char *buf, int size, const char *str, const char *what, const char *to)
{
	int i, b;
	int len = strlen(what);


	for (i = b = 0; str[i] != 0 && b < size - 1; i++, b++)
	{
		if (strncmp(&str[i], what, len) == 0)
		{
			const char *t = to;
			while ((buf[b++] = *(t++)));
			i += len - 1;
			b -= 2;
		}
		else
		{
			buf[b] = str[i];
		}
	}

	buf[b] = 0;
}



#define BUF_SIZE	1024

void make_dir (const char *dir)
{
	char buf[BUF_SIZE];
	int i = 0;


	do
	{
		buf[i] = dir[i];

		if (dir[i] == '/' || dir[i] == 0)
		{
			buf[i] = 0;
			mkdir (buf, 0750);
			buf[i] = '/';
		}
	} while (dir[i++] != 0 && i < BUF_SIZE);
}

