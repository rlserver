#ifndef __RLS_STATS__
#define __RLS_STATS__

struct session_info;
struct game_info;

void update_stats (const struct game_info *game, const struct session_info *sess);

#endif
