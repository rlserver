#ifndef __RLS_UTIL__
#define __RLS_UTIL__

void str_replace (char *buf, int size, const char *str, const char *what, const char *to);

/*
 * create directory (at multiple levels, if needed)
 * NB! assume dir[0] != 0
 */
void make_dir (const char *dir);

#endif

