#ifndef __RLS_SESSIONS__
#define __RLS_SESSIONS__

#include <time.h>
#include <pthread.h>
#include "users.h"
#include "games.h"

struct vt100;

typedef struct session_info
{
	int child_pid;			// or 0
	int socket;
	int user_id;			// or -1
	char user_name[USER_NAME_LEN];
	char game[GAME_NAME_LEN];
	time_t last_activity;		// to display idle time

	int term_wid, term_hgt;		// terminal size
	struct vt100 *term;
	int pty_master;			// forked processes should close this

	int observer_count;		// prohibit observing if -1
	int *observers;			// array of sockets

	pthread_mutex_t mutex;		// used to lock one session only

	// for the current game, not for the whole session
	struct
	{
		time_t start_time;		// when the game was started
		int bytes_in, bytes_out;	// not including observers
	} game_stat;

	struct session_info *prev, *next;
} session_info;


session_info* add_session (int sock);
// remove session from the list, socket and master_pty are not closed
// socket is removed from observers of all other sessions
void remove_session (session_info *sess);

// check if user is already logged in
int user_is_connected (int id);

int add_observer (int n, int sock);
void stop_observing (session_info *sess);

// send SIGTERM to all running games
void terminate_games (void);

// NB! not protected by mutex
void close_sessions (void);
// NB! not protected by mutex
void release_sessions (void);

#endif
