#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "log.h"
#include "games.h"

#define GAME_FILE	"./games"



static game_info* parse_game_begin (const char *buf, int ln)
{
	game_info *game;
	int n = strlen(buf + 1) - 1;


	if (buf[n] != ']')
	{
		write_log (LOG_ERROR, GAME_FILE ":%d: parse error", ln);
		return NULL;
	}
	if (n >= GAME_ID_LEN)
	{
		write_log (LOG_ERROR, GAME_FILE ":%d: game id is too long", ln);
		return NULL;
	}

	game = (game_info*)malloc(sizeof(game_info));
	if (game == NULL)
	{
		write_log (LOG_ERROR, "can't allocate memory for the game list");
		return NULL;
	}

	memset (game, 0, sizeof(game_info));
	memcpy (game->id, buf + 1, n - 1);
	game->id[n - 1] = 0;
	game->enter = '\n';

	return game;
}



enum
{
	G_NAME = 0,
	G_CHDIR,
	G_RUN,
	G_SCORE,
	G_COPY,
	G_COPYEDIT,
	G_ENTER,
	G_TOTAL
};

static int parse_game_line (game_info *game, const char *buf, int ln)
{
	static const char* const tags[G_TOTAL] = {"name:", "chdir:", "run:", "score:", "copy:", "copy-edit:", "enter:"};
	static const int lengths[G_TOTAL] = {GAME_NAME_LEN, GAME_DIR_LEN, GAME_COMMAND_LEN, GAME_COMMAND_LEN, GAME_FILE_LEN, GAME_FILE_LEN, 3};
	int taglen, len, i, j;


	if (game == NULL)
	{
		write_log (LOG_ERROR, GAME_FILE ":%d: no game is defined before data", ln);
		return -1;
	}

	for (i = 0; i < G_TOTAL; i++)
	{
		taglen = strlen(tags[i]);
		if (memcmp(buf, tags[i], taglen) == 0) break;
	}
	if (tags[i] == NULL)
	{
		write_log (LOG_ERROR, GAME_FILE ":%d: parse error", ln);
		return -1;
	}

	// skip whitespaces after colon
	while (isspace(buf[taglen]) && buf[taglen] != 0) taglen++;

	len = strlen(&buf[taglen]);
	if (buf[taglen + len - 1] == '\n') len--;	// subtract 1 for \n at the end
	if (len >= lengths[i])
	{
		write_log (LOG_ERROR, GAME_FILE ":%d: value is too long (max %d)", ln, lengths[i]);
		return -1;
	}

	switch (i)
	{
		case G_NAME:
			memcpy (game->name, &buf[taglen], len);
			game->name[len] = 0;
			break;
		case G_CHDIR:
			memcpy (game->dir, &buf[taglen], len);
			game->dir[len] = 0;
			break;
		case G_RUN:
			memcpy (game->cmd, &buf[taglen], len);
			game->cmd[len] = 0;
			break;
		case G_SCORE:
			memcpy (game->cmd_score, &buf[taglen], len);
			game->cmd_score[len] = 0;
			break;
		case G_COPY:
		case G_COPYEDIT:
			for (j = 0; game->files[j].file[0] != 0; j++)
			{
				if (j >= GAME_FILES)
				{
					write_log (LOG_ERROR, GAME_FILE ":%d: only %d files can be specified for a game", ln, GAME_FILES);
				}
			}

			memcpy (game->files[j].file, &buf[taglen], len);
			game->files[j].file[len] = 0;

			game->files[j].copy = strchr(game->files[j].file, ' ');
			if (game->files[j].copy == NULL || *(game->files[j].copy + 1) == 0)
			{
				write_log (LOG_ERROR, GAME_FILE ":%d: two file names should be specified, separated by a single space", ln);
				return -1;
			}
			*game->files[j].copy = 0;
			game->files[j].copy++;

			game->files[j].allow_edit = (i == G_COPYEDIT);
			break;
		case G_ENTER:
			if (strncasecmp(&buf[taglen], "cr", len) == 0) game->enter = '\r';
			else if (strncasecmp(&buf[taglen], "lf", len) == 0) game->enter = '\n';
			else write_log (LOG_ERROR, GAME_FILE ":%d: unknown value for enter key", ln);
			break;
		default:
			// should never happen
			return -1;
	}

	return 0;
}



// NB! game may be freed
static void add_game (game_info **gamelist, game_info *game, game_info **prev)
{
	// check if all required fields are defined
	if (!game->name[0] || !game->cmd[0])
	{
		write_log (LOG_ERROR, "game [%s] is missing 'name:' or 'run:'", game->id);
		free (game);
		return;
	}

	game->next = NULL;
	if (*gamelist == NULL) *gamelist = game;
	if (*prev != NULL) (*prev)->next = game;
	game->prev = *prev;
	*prev = game;
}



#define BUF_SIZE	1 * 1024

game_info* load_gamelist (void)
{
	game_info *gamelist = NULL, *game = NULL, *lg = NULL;
	char buf[BUF_SIZE];
	FILE *f;
	int ln = 1, err = 0;


	if ((f = fopen(GAME_FILE, "r")) == NULL)
	{
		write_log (LOG_ERROR, "can't open file \"" GAME_FILE "\"");
		return NULL;
	}

	while (1)
	{
		// end of file
		if (fgets(buf, BUF_SIZE, f) == NULL) break;

		// skip comments and empty strings
		if (buf[0] == '#' || buf[0] == '\n' || buf[0] == 0)
		{
			ln++;
			continue;
		}

		// game section start
		if (buf[0] == '[')
		{
			// add previous game to the list
			if (game != NULL) add_game (&gamelist, game, &lg);

			game = parse_game_begin(buf, ln);
			if (game == NULL)
			{
				err = 1;
				break;
			}
		}
		else if (parse_game_line(game, buf, ln) == -1)
		{
			err = 1;
			break;
		}

		ln++;
	}

	// add game to the list
	if (game != NULL) add_game (&gamelist, game, &lg);

	fclose (f);

	if (err)
	{
		free_gamelist (gamelist);
		return NULL;
	}

	return gamelist;
}



void free_gamelist (game_info *gamelist)
{
	game_info *g = gamelist;

	while (g != NULL)
	{
		game_info *g2 = g;

		g = g->next;

		free (g2);
	}
}


