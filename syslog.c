// do not include due to conflict of LOG_XXX constants
// #include "log.h"

#ifdef USE_SYSLOG


// This string is prepended to every message by syslog
#define IDENT_PREFIX	"rlserver"
// generic user-level messages
#define FACILITY	LOG_USER

#include <stdarg.h>
#include <syslog.h>


void init_log (void)
{
	openlog (IDENT_PREFIX, LOG_PID, FACILITY); 
}



void write_log (int level, const char *fmt, ...)
{
	va_list ap;


	// translate our levels to syslog
	static const int levels[5] = {LOG_DEBUG, LOG_INFO, LOG_WARNING, LOG_ERR, LOG_CRIT};


	va_start (ap, fmt);
	vsyslog (levels[level], fmt, ap);
	va_end (ap);
}



#endif

