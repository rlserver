#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libgen.h>
#include <time.h>
#include "util.h"
#include "log.h"
#include "server.h"
#include "games.h"
#include "sessions.h"


#define BUF_SIZE	1024

extern server_config config;



static void update_stat_file (const char *fn, const session_info *sess, time_t now)
{
	FILE *f;
	char buf[BUF_SIZE];
	int stats[4] = {1, now - sess->game_stat.start_time, sess->game_stat.bytes_in, sess->game_stat.bytes_out};
	int newfile = 0, n;


	if (fn[0] == 0) return;

	// make directory for it
	strcpy (buf, fn);
	make_dir (dirname(buf));


	if ((f = fopen(fn, "r+")) == NULL)
	{
		write_log (LOG_ERROR, "creating stat file %s", fn);

		newfile = 1;
		if ((f = fopen(fn, "w")) == NULL)
		{
			write_log (LOG_ERROR, "error creating %s", fn);
			return;
		}
	}

	// lock file to block access from other threads
	if (lockf(fileno(f), F_LOCK, 0) != 0)
	{
		write_log (LOG_ERROR, "error locking %s", fn);
		fclose (f);
		return;
	}

	// load old stats
	if (!newfile)
	{
		int i;

		for (i = 0; i < 4; i++)
		{
			if (fgets(buf, BUF_SIZE, f) == NULL)
			{
				write_log (LOG_ERROR, "error reading %s", fn);
				fclose (f);
				return;
			}

			stats[i] += atoi(buf);
		}

		fseek (f, SEEK_SET, 0);
	}

	// write updated data
	n = sprintf(buf, "%d\n%d\n%d\n%d\n", stats[0], stats[1], stats[2], stats[3]);
	if (fwrite(buf, n, 1, f) != 1) write_log (LOG_ERROR, "error writing %s", fn);

	fclose (f);
}



static void append_log_file (const char *fn, const session_info *sess, time_t now)
{
	FILE *f;
	char buf[BUF_SIZE], tm[100];
	struct tm tnow;
	int n;


	if (fn[0] == 0) return;

	// make directory for it
	strcpy (buf, fn);
	make_dir (dirname(buf));


	if ((f = fopen(fn, "a")) == NULL)
	{
		write_log (LOG_ERROR, "error opening %s");
		return;
	}

	// lock file to block access from other threads
	if (lockf(fileno(f), F_LOCK, 0) != 0)
	{
		write_log (LOG_ERROR, "error locking %s", fn);
		fclose (f);
		return;
	}

	// append session data
	strftime (tm, 100, "%F %H:%M:%S", localtime_r(&now, &tnow));
	n = sprintf(buf, "%s\t%s\t%d\t%d\t%d\n", tm, sess->user_name, (int)(now - sess->game_stat.start_time), sess->game_stat.bytes_in, sess->game_stat.bytes_out);
	if (fwrite(buf, n, 1, f) != 1) write_log (LOG_ERROR, "error writing %s", fn);

	fclose (f);
}



void update_stats (const game_info *game, const session_info *sess)
{
	char fn[BUF_SIZE];
	time_t now = time(NULL);


	str_replace (fn, BUF_SIZE, config.game_stat_file, "$GAME$", game->id);
	update_stat_file (fn, sess, now);

	str_replace (fn, BUF_SIZE, config.game_log_file, "$GAME$", game->id);
	append_log_file (fn, sess, now);
}

