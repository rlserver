#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <pthread.h>
#include <time.h>
#include "util.h"
#include "telnet.h"
#include "users.h"
#include "games.h"
#include "server.h"
#include "run.h"
#include "log.h"


extern server_config config;

extern session_info *sessionlist;
extern pthread_mutex_t sessionlist_mutex;

#define BUF_SIZE	1 * 1024


// return 1 if enter is pressed
static int input (char c, char *str, int *pos, int len, char *out, int *out_pos)
{
	// enter
	if (c == '\n')
	{
		str[*pos] = 0;
		out[*out_pos] = 0;
		return 1;
	}

	// backspace
	if (c == 8 || c == 127)
	{
		if (*pos > 0) *pos -= 1;

		out[*out_pos] = 8;
		if (*out_pos < len) *out_pos += 1;
	}
	else if (*pos < len - 1)
	{
		str[*pos] = c;
		if (*pos < len) *pos += 1;

		out[*out_pos] = c;
		if (*out_pos < len) *out_pos += 1;
	}

	return 0;
}



static int recv_input (session_info *sess, char *inp, int len)
{
	char buf[BUF_SIZE], out[BUF_SIZE];
	int pos = 0;


	while (1)
	{
		int opos = 0;

		int n = recv(sess->socket, buf, BUF_SIZE, 0);
		if (n == -1) return -1;

		n = translate_telnet_input(buf, n, sess);
		if (n > 0)
		{
			int i;
			for (i = 0; i < n; i++)
			{
				if (input(buf[i], inp, &pos, len, out, &opos))
				{
					return pos;
				}
			}
		}

		// echo
		if (send(sess->socket, out, opos, 0) == -1) return -1;
	}
}



static int print_and_wait (session_info *sess, const char *msg)
{
	char buf[BUF_SIZE];


	if (send(sess->socket, msg, strlen(msg) + 1, 0) == -1) return -1;

	while (1)
	{
		// wait for a keypress
		int n = recv(sess->socket, buf, BUF_SIZE, 0);
		if (n == -1) return -1;

		if (translate_telnet_input(buf, n, sess) > 0) break;
	}

	return 0;
}


#define REG_INPUTS	3

static int register_menu (session_info *sess)
{
	char data[REG_INPUTS][BUF_SIZE];
	int id, r = 0;
	const char *const prompts[REG_INPUTS] =
	{
		"\r\nUser name (3-16 chars, alphabet and digits only, start with a letter): ",
		"\r\nPassword (4-16 chars, it is INSECURE, so do not use the same as email): ",
		"\r\nEmail (optional): "
	};
	int lengths[REG_INPUTS][2] = { {MIN_USER_NAME_LEN, USER_NAME_LEN}, {MIN_USER_PASS_LEN, USER_PASS_LEN}, {0, USER_EMAIL_LEN} };
	char buf[BUF_SIZE];
	user usr;


	// ask user for data
	while (r < REG_INPUTS)
	{
		int len;

		if (send(sess->socket, prompts[r], strlen(prompts[r]), 0) == -1) return -1;

		len = recv_input(sess, data[r], lengths[r][1]);
		if (len < 0) return -1;

		if (len < lengths[r][0])
		{
			int l = sprintf(buf, "\r\nminimum %d symbols, try again: ", lengths[r][0]);
			if (send(sess->socket, buf, l, 0) == -1) return -1;
			continue;
		}

		r++;
	}


	// try to register using the data
	id = add_user(&usr, data[0], data[1], data[2]);
	if (id > 0)
	{
		int l;

		sess->user_id = usr.id;
		strcpy (sess->user_name, usr.name);

		write_log (LOG_DEBUG, "registered user %s", usr.name);

		l = sprintf(buf, "\r\nRegistration succeded\r\n");

		if (send(sess->socket, buf, l, 0) == -1) return -1;
	}
	else
	{
		int l;
		if (id == -1)
		{
			l = sprintf(buf, "\r\nInvalid name\r\n");
		}
		else if (id == -4)
		{
			l = sprintf(buf, "\r\nUser with such name already exists\r\n");
		}
		else
		{
			write_log (LOG_ERROR, "user registration failed, error %d", id);

			l = sprintf(buf, "\r\nRegistration failed\r\n");
		}

		if (send(sess->socket, buf, l, 0) == -1) return -1;
	}

	// wait for a keypress
	int n = recv(sess->socket, buf, BUF_SIZE, 0);
	if (n == -1) return -1;

	return 0;
}



static int login_menu (session_info *sess)
{
	const char login_prompt[] = "\r\nUser name: ";
	const char pass_prompt[] = "\r\nPassword: ";
	char name[USER_NAME_LEN], pass[USER_PASS_LEN];
	int id = 0;
	user usr;


	if (send(sess->socket, login_prompt, sizeof(login_prompt) - 1, 0) == -1) return -1;
	if (recv_input(sess, name, USER_NAME_LEN) < 0) return -1;

	if (send(sess->socket, pass_prompt, sizeof(pass_prompt) - 1, 0) == -1) return -1;
	if (recv_input(sess, pass, USER_PASS_LEN) < 0) return -1;

	// check user name and password
	if (check_user_login(&usr, name, pass) < 0)
	{
		if (print_and_wait(sess, "\r\nLogin failed\r\n") == -1) return -1;
		return -2;
	}

	// disallow simultaneous connections
	if (user_is_connected(usr.id))
	{
		if (print_and_wait(sess, "\r\nYou are already logged in\r\n") == -1) return -1;
		return -3;
	}

	sess->user_id = usr.id;
	strcpy (sess->user_name, usr.name);
	write_log (LOG_DEBUG, "user %s logs in", usr.name);

	return id;
}



static int passwd_menu (session_info *sess)
{
	const char old_prompt[] = "\r\nOld Password: ";
	const char new_prompt[] = "\r\nNew Password: ";
	char pass[USER_PASS_LEN];
	user usr;
	int len;


	// ask for old password first
	if (send(sess->socket, old_prompt, sizeof(old_prompt) - 1, 0) == -1) return -1;
	if (recv_input(sess, pass, USER_PASS_LEN) < 0) return -1;

	// check old password
	if (check_user_login(&usr, sess->user_name, pass) < 0)
	{
		if (print_and_wait(sess, "\r\nWrong password\r\n") == -1) return -1;
		return -2;
	}

	// ask for new password
	if (send(sess->socket, new_prompt, sizeof(new_prompt) - 1, 0) == -1) return -1;
	len = recv_input(sess, pass, USER_PASS_LEN);
	if (len < 0) return -1;

	if (len < MIN_USER_PASS_LEN)
	{
		if (print_and_wait(sess, "\r\nNew password is too short\r\n") == -1) return -1;
		return -3;
	}

	if (change_password(sess->user_id, pass) == 0)
	{
		if (print_and_wait(sess, "\r\nPassword has been changed\r\n") == -1) return -1;
	}

	return 0;
}



static int observe (session_info *sess)
{
	uint8_t buf[BUF_SIZE];


	while (1)
	{
		int n = recv(sess->socket, buf, BUF_SIZE, 0);
		if (n == -1) return -1;

		if (buf[0] == 'q') break;
	}

	return 0;
}



static int observer_menu (session_info *sess)
{
	char header[] = TELNET_CLS "\tGame\t\t\t\tUser\t\t  Idle\t\tTerminal\r\n";
	char buf[BUF_SIZE];
	session_info *s;
	int n, l, gn, count = 0;
	time_t now;

	char inp[20];


	if (send(sess->socket, header, sizeof(header) - 1, 0) == -1) return -1;

	pthread_mutex_lock (&sessionlist_mutex);
	now = time(NULL);
	for (s = sessionlist; s != NULL; s = s->next)
	{
		int idle;

		if (s->game[0] == 0 || s->observer_count == -1) continue;

		idle = now - s->last_activity;

		l = sprintf(buf, "%3d\t%s", count + 1, s->game);
		for (; l < 4 * 8; l++) buf[l] = ' ';

		l += sprintf(&buf[l], "\t%s", s->user_name);
		for (; l < 6 * 8; l++) buf[l] = ' ';

		l += sprintf(&buf[l], "\t%3dm %02ds\t%dx%d\r\n", idle / 60, idle % 60, s->term_wid, s->term_hgt);
		if (send(sess->socket, buf, l, 0) == -1) return -1;
		count++;
	}
	pthread_mutex_unlock (&sessionlist_mutex);
	l = sprintf(buf, "\r\n%d games total\r\n\r\n"
		"Input number to observe or anything else to return to the main menu\r\n"
		"You can press 'q' anytime to stop observing\r\n", count);
	if (send(sess->socket, buf, l, 0) == -1) return -1;


	// get number
	n = recv_input(sess, inp, 20);
	if (n == -1) return -1;

	gn = atoi(inp);
	if (gn > 0 && gn <= count)
	{
		// try to observe the specified game
		if (add_observer(gn - 1, sess->socket))
		{
			int r = observe(sess);
			stop_observing (sess);
			if (r == -1) return -1;
		}
	}

	return 0;
}



// if score == 1, display highscore list and exit rather than playing
static int play_menu (session_info *sess, int score)
{
	const char msg[] = TELNET_CLS "Select a game by entering its id\r\n";
	game_info *gamelist, *g;
	char buf[BUF_SIZE];
	int n;
	char inp[20];


	gamelist = load_gamelist();

	if (send(sess->socket, msg, sizeof(msg), 0) == -1)
	{
		free_gamelist (gamelist);
		return -1;
	}

	for (g = gamelist; g != NULL; g = g->next)
	{
		if (score && !g->cmd_score[0]) continue;

		int l = sprintf(buf, "%8s: %s\r\n", g->id, g->name);

		if (send(sess->socket, buf, l, 0) == -1)
		{
			free_gamelist (gamelist);
			return -1;
		}
	}


	// get id
	n = recv_input(sess, inp, 20);
	if (n == -1) return -1;

	// find and run game with such id
	for (g = gamelist; g != NULL; g = g->next)
	{
		if (score && !g->cmd_score[0]) continue;

		if (strcmp(inp, g->id) == 0)
		{
			// hack for displaying score
			if (score)
			{
				sess->observer_count = -1;
				strcpy (g->cmd, g->cmd_score);
			}

			run_game (sess, g);
			free_gamelist (gamelist);

			if (print_and_wait(sess, "\r\nYou have left the game\r\n") == -1) return -1;

			if (sess->observer_count == -1) sess->observer_count = 0;

			return 1;
		}
	}

	free_gamelist (gamelist);

	return 0;
}



static void run_editor (session_info *sess, const game_info *g, int i)
{
	game_info ed;
	char name[BUF_SIZE];
	int dirln;


	// prefix with game directory name
	if (g->dir[0]) dirln = sprintf(name, "%s/", g->dir);
	else dirln = 0;

	// copy file name to bufer
	strcat (&name[dirln], g->files[i].copy);

	// set up a a game struct for editor
	memset (&ed, 0, sizeof(game_info));
	strcpy (ed.id, "editor");
	strcpy (ed.name, "editor");
	ed.enter = '\n';
	snprintf (ed.cmd, GAME_COMMAND_LEN, "%s %s", config.editor, name);

	// disable observing
	sess->observer_count = -1;

	run_game (sess, &ed);

	// enable observing again
	sess->observer_count = 0;
}



static int edit_menu (session_info *sess)
{
	const char msg[] = TELNET_CLS "Select a file to edit by entering its number\r\n";
	game_info *gamelist, *g;
	char buf[BUF_SIZE], name[BUF_SIZE];
	int n, idx = 1;
	char inp[20];


	gamelist = load_gamelist();

	if (send(sess->socket, msg, sizeof(msg), 0) == -1)
	{
		free_gamelist (gamelist);
		return -1;
	}

	g = gamelist;
	while (g != NULL)
	{
		struct stat st;
		int l, i, dirln = 0;

		for (i = 0; i < GAME_FILES; i++)
		{
			if (!g->files[i].file[0]) break;
			if (!g->files[i].allow_edit) continue;

			// prefix with game directory name, expanding user name
			if (g->dir[0])
			{
				str_replace (name, BUF_SIZE, g->dir, "$USER$", sess->user_name);
				strcat (name, "/");
				dirln = strlen(name);
			}

			// copy file name to bufer, expanding user name
			str_replace (&name[dirln], BUF_SIZE, g->files[i].copy, "$USER$", sess->user_name);
			// file does not exist
			if (stat(name, &st) != 0) continue;

			l = sprintf(buf, "%3d: [%s] %s\r\n", idx, g->name, name);

			if (send(sess->socket, buf, l, 0) == -1)
			{
				free_gamelist (gamelist);
				return -1;
			}

			// we (ab)use the allow_edit to store index
			// to find the file when the user enters a number
			g->files[i].allow_edit = 0x80000000 | idx;

			idx++;
		}

		g = g->next;
	}


	// get id
	n = recv_input(sess, inp, 20);
	if (n == -1) return -1;

	n = 0x80000000 | atoi(inp);

	// find and run game with such id
	g = gamelist;
	while (g != NULL)
	{
		int i;

		for (i = 0; i < GAME_FILES; i++)
		{
			if (!g->files[i].file[0]) break;
			if (g->files[i].allow_edit != n) continue;

			run_editor (sess, g, i);

			free_gamelist (gamelist);

			return 0;
		}

		g = g->next;
	}

	free_gamelist (gamelist);

	return 0;
}



static int print_main_menu (session_info *sess)
{
	char msg[BUF_SIZE];
	const char *reg, *name, *play;


	// XXX this is ugly, clean it up someday

	if (sess->user_id < 0)
	{
		reg = "r) Register as a user (needed in order to play)";
		name = "player of roguelikes";
		play = "l) Log in to play";
	}
	else
	{
		reg = "c) Change password";
		name = sess->user_name;
		play = "p) Play\r\n\te) Edit game options files";
	}

	int l = snprintf(msg, BUF_SIZE, TELNET_CLS "Welcome to the roguelike server, %s!\r\n"
		"\t%s\r\n"
		"\to) Observe running games\r\n"
		"\ts) View high-score lists\r\n"
		"\t%s\r\n"
		"\tq) Leave\r\n", name, play, reg);

	if (send(sess->socket, msg, l, 0) == -1) return -1;
	return 0;
}



int main_menu (session_info *sess)
{
	if (print_main_menu(sess) == -1) return -1;

	while (1)
	{
		char buf[BUF_SIZE];
		int n = recv(sess->socket, buf, BUF_SIZE, 0);
		if (n == -1) return -1;

		n = translate_telnet_input(buf, n, sess);
		if (n > 0)
		{
			if (buf[0] == 'p' && sess->user_id >= 0)
			{
				if (play_menu(sess, 0) == -1) return -1;
				if (print_main_menu(sess) == -1) return -1;
			}

			if (buf[0] == 's')
			{
				if (play_menu(sess, 1) == -1) return -1;
				if (print_main_menu(sess) == -1) return -1;
			}

			if (buf[0] == 'e' && sess->user_id >= 0)
			{
				if (edit_menu(sess) == -1) return -1;
				if (print_main_menu(sess) == -1) return -1;
			}

			if (buf[0] == 'l' && sess->user_id < 0)
			{
				if (login_menu(sess) == -1) return -1;
				if (print_main_menu(sess) == -1) return -1;
			}

			if (buf[0] == 'r' && sess->user_id < 0)
			{
				if (register_menu(sess) == -1) return -1;
				if (print_main_menu(sess) == -1) return -1;
			}

			if (buf[0] == 'c' && sess->user_id >= 0)
			{
				if (passwd_menu(sess) == -1) return -1;
				if (print_main_menu(sess) == -1) return -1;
			}

			if (buf[0] == 'o')
			{
				if (observer_menu(sess) == -1) return -1;
				if (print_main_menu(sess) == -1) return -1;
			}

			if (buf[0] == 'q') return 0;
		}
	}
}

