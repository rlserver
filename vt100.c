#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "log.h"
#include "vt100.h"



vt100* init_term (int width, int height)
{
	vt100 *term = malloc(sizeof(vt100) + 3 * width * height);
	if (term == NULL)
	{
		write_log (LOG_ERROR, "unable to allocate memory to create terminal %dx%d", width, height);
		return NULL;
	}

	term->wid = width;
	term->hgt = height;
	term->cx = term->cy = 0;
	term->cmode = CM_NORM;
	term->att = 0;
	term->col = 7;
	term->esc_len = 0;
	term->prev_char = ' ';

	term->screen = ((uint8_t*)term) + sizeof(vt100);
	term->colour = term->screen + width * height;
	term->attrib = term->colour + width * height;
	memset (term->screen, ' ', width * height);
	memset (term->attrib, 0, width * height);
	memset (term->colour, 0, width * height);

	return term;
}



vt100* resize_term (vt100 *term, int width, int height)
{
	// XXX
	return term;
}



// we can't use strtol() because the string doesn't have to be 0-terminated
static int get_num (const uint8_t *data, int len, int *pos)
{
	int i, n = 0;


	for (i = 0; i < len; i++)
	{
		if (!isdigit(data[i])) break;
		n = n * 10 + data[i] - '0';
	}

	*pos = i;
	return n;
}



static void process_attr (vt100 *term, int attr)
{
	// attribute
	if (attr <= 10) term->att = attr;

	// foreground colour
	if (attr >= 30 && attr <= 37) term->col = (term->col & 0xF0) | (attr - 30);

	// background colour
	if (attr >= 40 && attr <= 47) term->col = (term->col & 0x0F) | ((attr - 40) << 4);
}



static void erase_screen (vt100 *term, int n1)
{
	memset (term->screen, ' ', term->wid * term->hgt);
	memset (term->attrib, 0, term->wid * term->hgt);
	memset (term->colour, 0, term->wid * term->hgt);
	if (n1 != 2) term->cx = term->cy = 0;
}



static void erase_line (vt100 *term, int n1)
{
	int ofs = term->wid * term->cy;
	int l = term->wid;

	// [ 0 K or [ K - to the end of line
	if (n1 <= 0)
	{
		ofs += term->cx;
		l -= term->cx;
	}

	// [ 1 K - to the beginning of line
	if (n1 == 1)
	{
		l = term->cx;
	}

	memset (&term->screen[ofs], ' ', l);
	memset (&term->attrib[ofs], 0, l);
	memset (&term->colour[ofs], 0, l);
}



static void erase_characters (vt100 *term, int n1)
{
	int ofs = term->wid * term->cy + term->cx;
	int l = term->wid - term->cx;

	if (n1 < l) l = n1;
	memset (&term->screen[ofs], ' ', l);
	memset (&term->attrib[ofs], 0, l);
	memset (&term->colour[ofs], 0, l);
}



// process single non-esc character
static void process_char (vt100 *term, char c)
{
	if (c == '\r')
	{
		term->cx = 0;
	}
	else if (c == '\n')
	{
		// XXX no scrolling
		if (term->cy < term->hgt - 1) term->cy++;
	}
	// backspace
	else if (c == 8)
	{
		if (term->cx > 0) term->cx--;
		return;
	}
	else
	{
		int ofs = term->cy * term->wid + term->cx;
		term->screen[ofs] = c;
		term->attrib[ofs] = term->att;
		term->colour[ofs] = term->col;
		if (++term->cx >= term->wid)
		{
			term->cx = 0;
			// XXX no scrolling
			if (term->cy < term->hgt - 1) term->cy++;
		}

		term->prev_char = c;
	}
}



#define MAX_ARGS	3

static int parse_dec (vt100 *term, int args, int arg[MAX_ARGS])
{
	int i;


	switch (term->esc[term->esc_len - 1])
	{
		// [ Y ; X H - goto Y,X
		case 'H':
		case 'f':
			if (args == 0)
			{
				term->cx = term->cy = 0;
				break;
			}

			if (args != 2) return -1;

			if (arg[0] != 0) arg[0]--;
			if (arg[0] >= 0 && arg[0] < term->hgt) term->cy = arg[0];
			if (arg[1] != 0) arg[1]--;
			if (arg[1] >= 0 && arg[1] < term->wid) term->cx = arg[1];
			break;

		// [ nn J or [ J - clear screen
		case 'J':
			if (args == 0) arg[0] = 0;
			erase_screen (term, arg[0]);
			break;

		// [ nn K or [ K - erase line
		case 'K':
			if (args == 0) arg[0] = 0;
			erase_line (term, arg[0]);
			break;

		// [ N1 m or [ N1 ; N2 m or [ N1 ; N2 ; N3 m - set colour and/or attribute (SGR mode)
		case 'm':
			if (args == 0) args = 1, arg[0] = 0;

			for (i = 0; i < args; i++)
			{
				process_attr (term, arg[i]);
			}
			break;

		// [ nn @ or [ @ - insert blank characters
		case '@':
			if (args == 0) arg[0] = 1;
			for (i = 0; i < arg[0]; i++)
			{
				process_char (term, ' ');
			}
			break;

		case 'b':
			if (args != 1) return -1;
			for (i = 0; i < arg[0]; i++)
			{
				process_char (term, term->prev_char);
			}
			break;

		// [ nn A or [ A - move cursor up
		case 'A':
			if (args == 0 || arg[0] < 1) arg[0] = 1;
			term->cy -= arg[0];
			if (term->cy < 0) term->cy = 0;
			break;

		// [ nn B or [ B - move cursor down
		case 'B':
			if (args == 0 || arg[0] < 1) arg[0] = 1;
			term->cy += arg[0];
			if (term->cy >= term->hgt) term->cy = term->hgt - 1;
			break;

		// [ nn C or [ C - move cursor right
		case 'C':
			if (args == 0 || arg[0] < 1) arg[0] = 1;
			term->cx += arg[0];
			if (term->cx >= term->wid) term->cx = term->wid - 1;
			break;

		// [ nn D or [ D - move cursor left
		case 'D':
			if (args == 0 || arg[0] < 1) arg[0] = 1;
			term->cx -= arg[0];
			if (term->cx < 0) term->cx = 0;
			break;

		// [ nn X - delete characters
	 	case 'X':
			if (args != 1) return -1;

			if (arg[0] == 0) arg[0] = 1;
			erase_characters (term, arg[0]);
			break;

		// [ nn d - move to specified row
		case 'd':
			if (args != 1) return -1;

			if (arg[0] != 0) arg[0]--;
			term->cy = arg[0];
			if (term->cy >= term->hgt) term->cy = term->hgt - 1;
			break;

		// [ nn G - move to specified column
	 	case 'G':
			if (args != 1) return -1;

			if (arg[0] != 0) arg[0]--;
			term->cx = arg[0];
			if (term->cx >= term->wid) term->cx = term->wid - 1;
			break;

		default:
			return -1;
	}

	return 0;
}



static int parse_dec_priv (vt100 *term, int args, int arg[MAX_ARGS])
{
	switch (term->esc[term->esc_len - 1])
	{
		case 'h':
			if (args == 0) return -1;
			if (arg[0] == 25) term->cmode = CM_NORM;
			break;

		case 'l':
			if (args == 0) return -1;
			if (arg[0] == 25) term->cmode = CM_HIDE;
			break;

		default:
			return -1;
	}

	return 0;
}



static int parse_esc (vt100 *term)
{
	int arg[MAX_ARGS];
	int p = 2, args = 0;


	// XXX something unknown - skip
	if (!isalnum(term->esc[2]))
	{
		if (term->esc[2] == '@' || term->esc[2] == '?') p++;
		else return -1;
	}

	// parse numbers
	while (args < MAX_ARGS)
	{
		int np;

		arg[args] = get_num(&term->esc[p], term->esc_len - p, &np);
		if (np == 0) break;

		p += 1 + np;
		args++;
		if (term->esc[p - 1] != ';') break;
	}

	// DEC command
	if (term->esc[1] == '[')
	{
		// DEC private mode command
		if (term->esc[2] == '?') return parse_dec_priv(term, args, arg);

		return parse_dec(term, args, arg);
	}
	
	return -1;
}



static void process_esc (vt100 *term)
{
	char c = term->esc[term->esc_len - 1];

	// sequence is not complete yet
	if (!isalpha(c) && c != '@' && c != 033) return;
	if (term->esc_len > 2) parse_esc (term);

	term->esc_len = 0;
}



void term_process (vt100 *term, const char *data, int len)
{
	int i, j;


	for (i = 0; i < len; i++)
	{
		// inside esc seqence
		if (term->esc_len)
		{
			term->esc[term->esc_len++] = data[i];
			process_esc (term);

			// XXX sequence is too long, just dump it to the terminal
			if (term->esc_len >= VT100_ESC_BUF)
			{
				for (j = 0; j < VT100_ESC_BUF; j++)
				{
					process_char (term, term->esc[j]);
				}
			}

			continue;
		}

		// esc
		if (data[i] == 033) term->esc[0] = 0x33, term->esc_len = 1;
		// a normal character
		else process_char (term, data[i]);
	}
}



#define BUFSZ	1280

static void add_buf (char *buf, int *pos, const char *data, int len, int fd)
{
	int l, max;


	while (len > 0)
	{
		max = BUFSZ - *pos;
		l = (len < max) ? len : max;
		memcpy (&buf[*pos], data, l);
		*pos = *pos + l;
		len -= l;
		data += l;

		if (*pos == BUFSZ)
		{
			write (fd, buf, BUFSZ);
			*pos = 0;
		}
	}
}



void term_copy_data (vt100 *term, int fd)
{
	char buf[BUFSZ] = "\033[H\033[2J";
	int i, pos = 7, a, c, cf, cb;
	char b2[80];
	int p2;


	// initial colours and attribute
	a = term->attrib[0];
	c = term->colour[0];
	cf = (c & 0x0F);
	cb = (c >> 4);
	pos += sprintf(&buf[pos], "\033[0;%dm\033[%d;%dm", a, cb + 40, cf + 30);


	for (i = 0; i < term->wid * term->hgt; i++)
	{
		int cf2, cb2;
		p2 = 0;

		// change attribute
		if (a != term->attrib[i])
		{
			a = term->attrib[i];
			p2 += sprintf(&b2[p2], "\033[0;%dm", a);

			// force colour change since colours are reset
			c = cf = cb = -1;
		}

		// change colour
		if (c != term->colour[i])
		{
			c = term->colour[i];
			cf2 = (c & 0x0F);
			cb2 = (c >> 4);

			if (cf2 != cf && cb2 != cb) p2 += sprintf(&b2[p2], "\033[%d;%dm", cb2 + 40, cf2 + 30);
			else if (cf2 != cf) p2 += sprintf(&b2[p2], "\033[%dm", cf2 + 30);
			else p2 += sprintf(&b2[p2], "\033[%dm", cb2 + 40);

			cb = cb2;
			cf = cf2;
		}

		add_buf (buf, &pos, b2, p2, fd);

		add_buf (buf, &pos, (char*)&term->screen[i], 1, fd);
	}

	p2 = sprintf(b2, "\033[%d;%dH", term->cy + 1, term->cx + 1);
	add_buf (buf, &pos, b2, p2, fd);

	// cursor state
	if (term->cmode == CM_HIDE) p2 = sprintf(b2, "\033[?25l");
	else p2 = sprintf(b2, "\033[?25h");
	add_buf (buf, &pos, b2, p2, fd);

	if (pos > 0) write (fd, buf, pos);
}

