#ifndef __USERS__
#define __USERS__

#define USER_NAME_LEN	16
#define USER_PASS_LEN	16
#define USER_EMAIL_LEN	32

#define MIN_USER_NAME_LEN	3
#define MIN_USER_PASS_LEN	4

typedef struct
{
	int id;
	char name[USER_NAME_LEN];
	char pass[USER_PASS_LEN];
	char email[USER_EMAIL_LEN];
} user;


int add_user (user *usr, const char *name, const char *pass, const char *email);
int check_user_login (user *usr, const char *name, const char *pass);
int change_password (int user_id, const char *pass);


#endif

