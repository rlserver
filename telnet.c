#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <pty.h>
#include "sessions.h"
#include "telnet.h"


int translate_telnet_input (char *cbuf, int len, session_info *sess)
{
	uint8_t * const buf = (uint8_t*)cbuf;


	int i = 0, j = 0;
	while (i < len)
	{
		// CR
		if (buf[i] == '\r')
		{
			// translate it to LF
			buf[j++] = '\n';

			if (++i >= len) break;	// XXX

			// CR LF and CR 0 are translated to LF
			// translation is already done, so we only have to skip it here
			if (buf[i] == '\n' || buf[i] == 0) i++;

			continue;
		}

		// telnet command
		if (buf[i] == TN_IAC)
		{
			if (++i >= len) break;	// XXX

			// it's just an escaped data octet
			if (buf[i] == TN_IAC)
			{
				buf[j++] = buf[i++];
				continue;
			}

			// some confirmation
			if (buf[i] == TN_WILL || buf[i] == TN_DO)
			{
				i += 2;		// XXX just skip it for now
			}

			// something is rejected
			if (buf[i] == TN_WONT || buf[i] == TN_DONT)
			{
				i += 2;		// XXX just skip it for now
			}

			// some subsection begins
			if (buf[i] == TN_SB)
			{
				if (++i >= len) break;  // XXX

				// terminal window size
				if (buf[i] == TN_NAWS)
				{
					struct winsize ws;
					int n1, n2;

					i++;

					n1 = buf[i++];
					if (n1 == TN_IAC) n1 = buf[i++];
					n2 = buf[i++];
					if (n2 == TN_IAC) n2 = buf[i++];
					ws.ws_col = (n1 << 8) | n2;

					n1 = buf[i++];
					if (n1 == TN_IAC) n1 = buf[i++];
					n2 = buf[i++];
					if (n2 == TN_IAC) n2 = buf[i++];
					ws.ws_row = (n1 << 8) | n2;

					if (i + 2 > len) break;	// XXX

					// skip IAC SE
					i += 2;
					if (sess != NULL)
					{
						if (sess->pty_master >= 0)
						{
							ioctl (sess->pty_master, TIOCSWINSZ, &ws);
						}

						// do not lock mutex, it should be safe
						sess->term_wid = ws.ws_col;
						sess->term_hgt = ws.ws_row;
					}

					continue;
				}

				// XXX
			}

			continue;
		}

		// an ordinary octet, just copy it
		buf[j++] = buf[i++];
	}

	return j;
}



void send_telnet_init (int sck)
{
	uint8_t req[] =
	{
		TN_IAC, TN_WILL, TN_SUPPRESS_GA, 	// enable full-duplex mode (RFC 858)
		TN_IAC, TN_DO, TN_NAWS, 		// request window size info (RFC 1073)
		TN_IAC, TN_WILL, TN_ECHO 		// request to disable local echo (RFC 857)
	};
	send (sck, req, sizeof(req), 0);
}

