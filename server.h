#ifndef __SERVER__
#define __SERVER__

#include <stdint.h>
#include "sessions.h"

#define CONFIG_LEN	128
typedef struct
{
	// command for external editor
	char editor[CONFIG_LEN];

	// port to listen
	uint16_t port;
	// how many pending connections queue will hold
	uint16_t backlog;

	// grace time in seconds before sending SIGTERM to game whose player disonnected
	uint16_t delay_before_kill;

	// user/group to set after init is done
	int uid, gid;

	// file name to save game stats
	char game_stat_file[CONFIG_LEN];
	// file name to save game session logs
	char game_log_file[CONFIG_LEN];
} server_config;

int main_menu (session_info *sess);


#endif
