#ifndef __RLS_VT100__
#define __RLS_VT100__

#include <stdint.h>


#define VT100_ESC_BUF		24

typedef struct vt100
{
	int wid, hgt;				// size
	int cx, cy;				// cursor position
	enum
	{
		CM_NORM = 0,
		CM_HIDE,
		CM_BIG
	} cmode;				// cursor mode
	int att, col;				// current attribute and colour
	uint8_t *screen, *colour, *attrib;	// screen data

	uint8_t esc[VT100_ESC_BUF];		// buffer for escape sequence
	int esc_len;

	uint8_t prev_char;
} vt100;


vt100* init_term (int width, int height);
vt100* resize_term (vt100 *term, int width, int height);
void term_process (vt100 *term, const char *data, int len);
void term_copy_data (vt100 *term, int fd);

#endif
