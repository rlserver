#ifndef __RLS_RUNGAME__
#define __RLS_RUNGAME__

struct session_info;

void run_game (session_info *sess, const game_info *game);

#endif
