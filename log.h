#ifndef __RLS_LOG__
#define __RLS_LOG__

enum
{
	LOG_DEBUG = 0,
	LOG_INFO,
	LOG_WARNING,
	LOG_ERROR,
	LOG_FATAL
};


void init_log (void);
void write_log (int level, const char *fmt, ...);


#endif

