#ifndef __RLS_TELNET__
#define __RLS_TELNET__

#include "sessions.h"

enum
{
	// RFC 854
	TN_IAC		= 0xFF,
	TN_WILL		= 0xFB,
	TN_WONT		= 0xFC,
	TN_DO		= 0xFD,
	TN_DONT		= 0xFE,
	TN_SB		= 0xFA,
	TN_SE		= 0xF0,
	// RFC 857
	TN_ECHO		= 0x01,
	// RFC 858
	TN_SUPPRESS_GA	= 0x03,
	// RFC 1073
	TN_NAWS		= 0x1F
};


#define TELNET_CLS	"\033[H\033[2J"		"\033[0m\033[40;37m"


void send_telnet_init (int sck);
int translate_telnet_input (char *cbuf, int len, session_info *sess);


#endif /*__RLS_TELNET__*/

