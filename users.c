#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/stat.h>
#include <unistd.h>
#include "log.h"
#include "users.h"

#define USERS_FILE "./users"


static int check_user_name (const char *name)
{
	int i;


	if (!isalpha(name[0])) return 0;

	for (i = 1; name[i] != 0; i++)
	{
		if (!isalnum(name[i])) return 0;
	}

	// minimal length
	return i >= 3;
}



/*
 * str must be 0-terminated
 * NB! content of str will be destroyed in the process
 */
static int parse_user (user *usr, char *str)
{
	char *s;
	if ((s = strstr(str, ":")) == NULL) return 0;
	*s = 0;

	// id
	usr->id = atoi(str);

	str = s + 1;
	if ((s = strstr(str, ":")) == NULL) return 0;
	*s = 0;

	// name
	strncpy (usr->name, str, USER_NAME_LEN);
	usr->name[USER_NAME_LEN - 1] = 0;

	str = s + 1;
	if ((s = strstr(str, ":")) == NULL) return 0;
	*s = 0;

	// password
	strncpy (usr->pass, str, USER_PASS_LEN);
	usr->name[USER_PASS_LEN - 1] = 0;

	str = s + 1;
	if ((s = strstr(str, "\n")) != NULL) *s = 0;	// remove optional \n at the end

	// email
	strncpy (usr->email, str, USER_EMAIL_LEN);
	usr->name[USER_EMAIL_LEN - 1] = 0;

	return 1;
}



#define BUF_SZ	1024

/*
 * if user is not found usr will be filled with garbage
 * 	and also if max_id is not NULL, it will contain max encountered user id
 */
static int find_user (const char *name, user *usr, FILE *f, int *max_id)
{
	int m = 0;

	while (1)
	{
		char buf[BUF_SZ];

		// end of file
		if (fgets(buf, BUF_SZ, f) == NULL) break;

		// skip comments and empty lines
		if (buf[0] == '#' || buf[0] == 0) continue;

		if (!parse_user(usr, buf))
		{
			continue;
		}

		// logins are case-insensitive
		if (strcasecmp(name, usr->name) == 0) return usr->id;

		// update max id
		if (usr->id > m) m = usr->id;
	}

	if (max_id != NULL) *max_id = m;
	return 0;
}



static void write_user (FILE *f, const user *usr)
{
	fprintf (f, "%d:%s:%s:%s\n", usr->id, usr->name, usr->pass, usr->email);
}



int add_user (user *usr, const char *name, const char *pass, const char *email)
{
	int id;
	FILE *f;

	if (!check_user_name(name)) return -1;

	if ((f = fopen(USERS_FILE, "r+")) == NULL)
	{
		write_log (LOG_ERROR, "error opening " USERS_FILE);
		return -2;
	}

	// lock file to block access from other threads
	if (lockf(fileno(f), F_LOCK, 0) != 0)
	{
		write_log (LOG_ERROR, "error locking " USERS_FILE);
		fclose (f);
		return -3;
	}

	// user already exists
	if (find_user(name, usr, f, &id))
	{
		fclose (f);
		return -4;
	}

	id++;
	usr->id = id;

	strncpy (usr->name, name, USER_NAME_LEN);
	usr->name[USER_NAME_LEN - 1] = 0;

	strncpy (usr->pass, pass, USER_PASS_LEN);
	usr->pass[USER_PASS_LEN - 1] = 0;

	strncpy (usr->email, email, USER_EMAIL_LEN);
	usr->email[USER_EMAIL_LEN - 1] = 0;

	// add user record to the file
	fseek (f, 0, SEEK_END);

	write_user (f, usr);

	fclose (f);

	return id;
}



int check_user_login (user *usr, const char *name, const char *pass)
{
	int id;
	FILE *f;


	f = fopen(USERS_FILE, "r+");	// r+ is needed for locking
	if (f == NULL)
	{
		write_log (LOG_ERROR, "error opening " USERS_FILE);
		return -2;
	}

	// lock file to block access from other threads
	if (lockf(fileno(f), F_LOCK, 0) != 0)
	{
		write_log (LOG_ERROR, "error locking " USERS_FILE);
		fclose (f);
		return -3;
	}

	id = find_user(name, usr, f, NULL);
	fclose (f);

	if (id <= 0) return -4;

	if (strcmp(pass, usr->pass) != 0) return -5;

	return id;
}



static void change_password_save (FILE *f, char *buf, int size, int user_id, const char *pass)
{
	char str[BUF_SZ];
	char *end = buf + size;


	while (buf <= end)
	{
		user usr;
		char *s = strstr(buf, "\n");

		if (s == NULL) s = end;
		else *s = 0;

		// copy string (parse_user is destructive)
		snprintf (str, BUF_SZ, buf);

		// not user or not this user
		// just copy the string
		if (!parse_user(&usr, str) || usr.id != user_id)
		{
			fprintf (f, "%s\n", buf);
		}
		// replace password and save user
		else
		{
			strncpy (usr.pass, pass, USER_PASS_LEN);
			usr.pass[USER_PASS_LEN - 1] = 0;
			write_user (f, &usr);
		}

		buf = s + 1;
	}
}



int change_password (int user_id, const char *pass)
{
	struct stat st;
	char *buf;
	FILE *f;


	if ((f = fopen(USERS_FILE, "r+")) == NULL)
	{
		write_log (LOG_ERROR, "error opening " USERS_FILE);
		return -2;
	}

	// lock file to block access from other threads
	if (lockf(fileno(f), F_LOCK, 0) != 0)
	{
		write_log (LOG_ERROR, "error locking " USERS_FILE);
		fclose (f);
		return -3;
	}

	// allocate buffer to store all users

	if (fstat(fileno(f), &st) == -1)
	{
		write_log (LOG_ERROR, "can't stat " USERS_FILE);
		fclose (f);
		return -4;
	}

	buf = (char*)malloc(st.st_size);
	if (buf == NULL)
	{
		write_log (LOG_ERROR, "unable to allocate memory to load " USERS_FILE);
		fclose (f);
		return -5;
	}

	// read the whole user file into memory
	fread (buf, st.st_size, 1, f);

	fseek (f, 0, SEEK_SET);
	change_password_save (f, buf, st.st_size, user_id, pass);

	fclose (f);
	free (buf);

	return 0;
}

