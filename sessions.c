#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/socket.h>
#include <pthread.h>
#include "vt100.h"
#include "sessions.h"



session_info *sessionlist = NULL;
int session_count = 0;
pthread_mutex_t sessionlist_mutex = PTHREAD_MUTEX_INITIALIZER;



session_info* add_session (int sock)
{
	session_info *ps = (session_info*)malloc(sizeof(session_info));
	if (ps == NULL) return NULL;

	// fill data
	ps->prev = NULL;
	ps->next = NULL;
	ps->child_pid = 0;
	ps->socket = sock;
	ps->user_id = -1;
	ps->user_name[0] = 0;
	ps->game[0] = 0;
	ps->term = NULL;
	ps->term_wid = 80;
	ps->term_hgt = 24;
	ps->pty_master = -1;
	ps->observer_count = 0;
	ps->observers = NULL;
	pthread_mutex_init (&ps->mutex, NULL);
	ps->last_activity = time(NULL);


	pthread_mutex_lock (&sessionlist_mutex);

	// add it to the list
	if (sessionlist == NULL)
	{
		sessionlist = ps;
	}
	else
	{
		session_info *p = sessionlist;
		while (p->next != NULL) p = p->next;
		p->next = ps;
		ps->prev = p;
	}
	session_count++;

	pthread_mutex_unlock (&sessionlist_mutex);

	return ps;
}



void remove_session (session_info *sess)
{
	session_info *s;

	pthread_mutex_lock (&sessionlist_mutex);
	pthread_mutex_lock (&sess->mutex);

	for (s = sessionlist; s != NULL; s = s->next)
	{
		// remove sess from the list
		if (s == sess)
		{
			if (s->next) s->next->prev = s->prev;
			if (s->prev) s->prev->next = s->next;

			if (sessionlist == sess) sessionlist = sess->next;
			session_count--;
		}
		// remove socket from observers
		else
		{
			int i;
			// no need to lock s->mutex because add_observer() is blocked by sessionlist_mutex
			for (i = 0; i < s->observer_count; i++)
			{
				if (s->observers[i] == sess->socket)
				{
					s->observers[i--] = s->observers[s->observer_count--];
				}
			}
		}
	}

	pthread_mutex_unlock (&sessionlist_mutex);

	pthread_mutex_unlock (&sess->mutex);
	pthread_mutex_destroy (&sess->mutex);
	free (sess->term);
	free (sess);
}



int user_is_connected (int id)
{
	session_info *s;

	pthread_mutex_lock (&sessionlist_mutex);

	for (s = sessionlist; s != NULL; s = s->next)
	{
		if (s->user_id == id)
		{
			pthread_mutex_unlock (&sessionlist_mutex);
			return 1;
		}
	}

	pthread_mutex_unlock (&sessionlist_mutex);
	return 0;
}



void stop_observing (session_info *sess)
{
	session_info *s;

	pthread_mutex_lock (&sessionlist_mutex);

	for (s = sessionlist; s != NULL; s = s->next)
	{
		// remove socket from observers
		int i;
		// no need to lock s->mutex because add_observer() is blocked by sessionlist_mutex
		for (i = 0; i < s->observer_count; i++)
		{
			if (s->observers[i] == sess->socket)
			{
				s->observers[i--] = s->observers[--s->observer_count];
			}
		}
	}

	pthread_mutex_unlock (&sessionlist_mutex);
}



int add_observer (int n, int sock)
{
	session_info *s;
	int i = 0, done = 0;

	pthread_mutex_lock (&sessionlist_mutex);

	for (s = sessionlist; s != NULL; s = s->next)
	{
		if (s->game[0] == 0 || s->observer_count == -1) continue;
		if (i++ != n) continue;

		pthread_mutex_lock (&s->mutex);
		s->observers = (int*)realloc(s->observers, (s->observer_count + 1) * sizeof(int));
		s->observers[s->observer_count++] = sock;
		pthread_mutex_unlock (&s->mutex);
		done = 1;
		break;
	}

	if (s->term != NULL) term_copy_data (s->term, sock);

	pthread_mutex_unlock (&sessionlist_mutex);
	return done;
}



void close_sessions (void)
{
	session_info *s;
	for (s = sessionlist; s != NULL; s = s->next)
	{
		if (s->pty_master != -1) close (s->pty_master);
		close (s->socket);
	}
}



void release_sessions (void)
{
	session_info *s;
	for (s = sessionlist; s != NULL; s = s->next)
	{
		free (s->observers);
		pthread_mutex_destroy (&s->mutex);
	}
}



void terminate_games (void)
{
	session_info *s;


	pthread_mutex_lock (&sessionlist_mutex);

	for (s = sessionlist; s != NULL; s = s->next)
	{
		if (!s->child_pid) continue;
		kill (s->child_pid, SIGTERM);
	}

	pthread_mutex_unlock (&sessionlist_mutex);
}

