#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <pthread.h>
#include <pwd.h>
#include <grp.h>
#include "telnet.h"
#include "sessions.h"
#include "server.h"
#include "log.h"

#define CONFIG_FILE	"config"

// socket used to listen for new connections
int server_socket = -1;

server_config config = {"ee", 3456, 3, 5, -1, -1, "", ""};



/*
 * scan system users for the specified name
 * returns uid or -1
 */
static int find_uid (const char *name)
{
	struct passwd *pwd;


	// rewind to the first entry
	setpwent ();

	// scan entries
	while ((pwd = getpwent()) != NULL)
	{
		if (strcmp(name, pwd->pw_name) == 0) return pwd->pw_uid;
	}

	// not found
	return -1;
}



/*
 * scan system groups for the specified name
 * returns uid or -1
 */
static int find_gid (const char *name)
{
	struct group *grp;


	// rewind to the first entry
	setgrent ();

	// scan entries
	while ((grp = getgrent()) != NULL)
	{
		if (strcmp(name, grp->gr_name) == 0) return grp->gr_gid;
	}

	// not found
	return -1;
}



static int parse_config_line (char *buf, int ln)
{
	char *val;
	int len;


	// find value after ':'
	val = strchr(buf, ':');
	if (val == NULL)
	{
		write_log (LOG_ERROR, CONFIG_FILE ":%d: parse error", ln);
		return -1;
	}

	// skip leading spaces
	do val++; while (isspace(val[0]));

	len = strlen(val);
	if (val[len - 1] == '\n') val[--len] = 0;	// subtract 1 for \n at the end
	if (len >= CONFIG_LEN)
	{
		write_log (LOG_ERROR, CONFIG_FILE ":%d: value is too long (max %d)", ln, CONFIG_LEN);
		return -1;
	}

	if (strncmp(buf, "port:", 5) == 0)
	{
		config.port = atoi(val);
		return 0;
	}

	if (strncmp(buf, "backlog:", 8) == 0)
	{
		config.backlog = atoi(val);
		return 0;
	}

	if (strncmp(buf, "editor:", 7) == 0)
	{
		memcpy (config.editor, val, len);
		return 0;
	}

	if (strncmp(buf, "kill_delay:", 11) == 0)
	{
		config.delay_before_kill = atoi(val);
		return 0;
	}

	if (strncmp(buf, "user:", 5) == 0)
	{
		if ((config.uid = find_uid(val)) == -1)
		{
			write_log (LOG_ERROR, CONFIG_FILE ":%d: invalid user %s", ln, val);
			return -1;
		}
		return 0;
	}

	if (strncmp(buf, "group:", 6) == 0)
	{
		if ((config.gid = find_gid(val)) == -1)
		{
			write_log (LOG_ERROR, CONFIG_FILE ":%d: invalid group %s", ln, val);
			return -1;
		}
		return 0;
	}

	if (strncmp(buf, "stats:", 6) == 0)
	{
		memcpy (config.game_stat_file, val, len);
		return 0;
	}

	if (strncmp(buf, "log:", 4) == 0)
	{
		memcpy (config.game_log_file, val, len);
		return 0;
	}

	write_log (LOG_ERROR, CONFIG_FILE ":%d: unknown parameter", ln);
	return -1;
}



#define CFG_BUF_SIZE	(CONFIG_LEN + 16)
static int load_config (void)
{
	char buf[CFG_BUF_SIZE];
	FILE *f;
	int ln = 1;


	if ((f = fopen(CONFIG_FILE, "r")) == NULL)
	{
		write_log (LOG_ERROR, "can't open config file: " CONFIG_FILE);
		return -1;
	}

	while (1)
	{
		// end of file
		if (fgets(buf, CFG_BUF_SIZE, f) == NULL) break;

		// skip comments and empty strings
		if (buf[0] == '#' || buf[0] == '\n' || buf[0] == 0)
		{
			ln++;
			continue;
		}

		if (parse_config_line(buf, ln) == -1)
		{
			fclose (f);
			return -1;
		}

		ln++;
	}

	fclose (f);
	return 0;
}



static void sigchld_handler (int s)
{
	while (waitpid(-1, NULL, WNOHANG) > 0);
}



static void sigterm_handler (int s)
{
	write_log (LOG_INFO, "initiating shutdown");

	// do not accept new connections
	close (server_socket);

	// terminate all running games
	terminate_games ();

	/* we do not really care if spawned threads have been finished
	   because all threads will be forced to close anyway when the main process exits
	   it is a little bit dirty, but is simple and works */

	extern pthread_mutex_t sessionlist_mutex;
	pthread_mutex_destroy (&sessionlist_mutex);

	// close and release all connections
	close_sessions ();
	release_sessions ();

	write_log (LOG_INFO, "shutdown complete");
	exit (0);
}



static void init_signals (void)
{
	struct sigaction sa;

	// SIGCHLD
	// prevent terminated child processes from becoming zombies
	sa.sa_handler = sigchld_handler;
	sigemptyset (&sa.sa_mask);
	sa.sa_flags = SA_RESTART | SA_NOCLDSTOP;
	if (sigaction(SIGCHLD, &sa, NULL) == -1)
	{
		write_log (LOG_FATAL, "error setting SIGCHLD handler");
		exit (1);
	}

	// SIGTERM
	// gracefully close all runing games and exit upon request
	sa.sa_handler = sigterm_handler;
	sigemptyset (&sa.sa_mask);
	sa.sa_flags = SA_RESTART;
	if (sigaction(SIGTERM, &sa, NULL) == -1)
	{
		write_log (LOG_FATAL, "error setting SIGTERM handler");
		exit (1);
	}
	// use the same handler for SIGINT
	if (sigaction(SIGINT, &sa, NULL) == -1)
	{
		write_log (LOG_FATAL, "error setting SIGINT handler");
		exit (1);
	}

	// SIGPIPE - ignore it (check return codes for errors instead)
	sa.sa_handler = SIG_IGN;
	sigemptyset (&sa.sa_mask);
	if (sigaction(SIGPIPE, &sa, NULL) == -1)
	{
		write_log (LOG_FATAL, "error setting SIGPIPE to ignore");
		exit (1);
	}
}



static int open_socket (int port)
{
	int sockfd;			// listen on sock_fd
	struct sockaddr_in my_addr;	// my address information
	int yes = 1;

	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
		write_log (LOG_FATAL, "unable to create socket");
		return -1;
	}

	if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
	{
		write_log (LOG_FATAL, "unable to set socket options");
		close (sockfd);
		return -1;
	}

	my_addr.sin_family = AF_INET;		// host byte order
	my_addr.sin_port = htons(port);		// short, network byte order
	my_addr.sin_addr.s_addr = INADDR_ANY;	// automatically fill with my IP
	memset (&(my_addr.sin_zero), 0, 8);	// zero the rest of the struct

	if (bind(sockfd, (struct sockaddr*)&my_addr, sizeof(struct sockaddr)) == -1)
	{
		write_log (LOG_FATAL, "unable to bind socket to port %d", port);
		close (sockfd);
		return -1;
	}

	if (listen(sockfd, config.backlog) == -1)
	{
		write_log (LOG_FATAL, "unable to listen on socket");
		close (sockfd);
		return -1;
	}

	return sockfd;
}



typedef struct
{
	int sock;
} thread_data;



/*
 * Handle newly connected client in a thread
 */
static void* connection_thread (void *data)
{
	session_info *sess;
	int r;
	int sock = ((thread_data*)data)->sock;
	free (data);

	send_telnet_init (sock);

	sess = add_session(sock);

	r = main_menu(sess);

	if (sess->user_name[0]) write_log (LOG_DEBUG, "user %s disconnected", sess->user_name);
	else write_log (LOG_DEBUG, "anonymous user disconnected");

	remove_session (sess);
	close (sock);
	return NULL;
}



static void handle_connect (int new_fd)
{
	pthread_attr_t thread_attr;
	thread_data *data;
	pthread_t th;


	if ((data = malloc(sizeof(thread_data))) == NULL)
	{
		close (new_fd);
		write_log (LOG_ERROR, "can not handle connection: not enough memory");
		return;
	}

	data->sock = new_fd;

	// init thread data
	pthread_attr_init (&thread_attr);
	pthread_attr_setdetachstate (&thread_attr, PTHREAD_CREATE_DETACHED);
	pthread_attr_setscope (&thread_attr, PTHREAD_SCOPE_PROCESS);

	// handle new connection in a thread
	if (pthread_create(&th, &thread_attr, connection_thread, data) != 0)
	{
		write_log (LOG_ERROR, "can not create thread to handle connection");
		free (data);
		close (new_fd);
	}
}



int main (void)
{
	// init things

	init_log ();
	write_log (LOG_INFO, "starting");

	if (load_config() == -1) return 2;

	init_signals ();

	server_socket = open_socket(config.port);
	if (server_socket == -1) return 1;


	// drop privilegies

	if (config.gid != -1)
	{
		if (setgid(config.gid) != 0)
		{
			write_log (LOG_FATAL, "can not set group id to %d\n", config.gid);
			return 3;
		}
	}

	if (config.uid != -1)
	{
		if (setuid(config.uid) != 0)
		{
			write_log (LOG_FATAL, "can not set user id to %d\n", config.uid);
			return 3;
		}
	}

	// had the user lost his mind?
	if (getuid() == 0)
	{
		write_log (LOG_FATAL, "running this software as root is strictly forbidden");
		return 3;
	}

	// detach from the terminal in a daemon style init
	if (fork()) return 0;
	close (0);
	close (1);
	close (2);

	write_log (LOG_INFO, "init complete, ready to serve");

	// the main loop
	while (1)
	{		
		struct sockaddr_in their_addr;	// connector's address information
		socklen_t sin_size = sizeof(struct sockaddr_in);
		int new_fd;

		// got a new connection
		if ((new_fd = accept(server_socket, (struct sockaddr*)&their_addr, &sin_size)) == -1)
		{
			write_log (LOG_ERROR, "unable to accept connection");
			continue;
		}

		write_log (LOG_DEBUG, "got connection from %s\n", inet_ntoa(their_addr.sin_addr));

		handle_connect (new_fd);
	}

	// code here is never supposed to execute
	// the program terminates via SIGTERM handler

	return 0;
}

