#ifndef __GAMES__
#define __GAMES__

#define GAME_ID_LEN		8
#define GAME_NAME_LEN		32
#define GAME_DIR_LEN		32
#define GAME_COMMAND_LEN	100
#define GAME_FILE_LEN		64

#define GAME_FILES		8


typedef struct game_info
{
	char id[GAME_ID_LEN];
	char name[GAME_NAME_LEN];
	char dir[GAME_DIR_LEN];
	char cmd[GAME_COMMAND_LEN];
	char cmd_score[GAME_COMMAND_LEN];
	char enter;				// what symbol to use for Enter key (\n or \r)
	struct
	{
		int allow_edit;
		char file[GAME_FILE_LEN];	// two file names are separated by 0
		char *copy;			// point inside str[]
	} files[GAME_FILES];

	struct game_info *prev, *next;
} game_info;


game_info* load_gamelist (void);
void free_gamelist (game_info *gamelist);


#endif
